﻿Imports System.Text
Imports LayerBDs
Imports LayerDO
Imports LayerENT

Module Program

    Private ReadOnly UserService As New UserService(New UserRepository())
    Private ReadOnly ProductService As New ProductService(New ProductRepository())
    Private _user As User

    Sub Main()
        Console.WriteLine("*** Nill Frank Flores CRUD - Console Demo *** {0}", Environment.NewLine)

        LogOnUser()

        If _user Is Nothing Then End

        ShowWelcome()
        ShowMenu()
    End Sub

    Private Sub LogOnUser()
        Dim attempts As Integer
        Do
            Dim userName = AskForUserName()
            Dim password = AskForPassword()

            If UserService.ValidateCredentials(userName, password) Then
                _user = UserService.GetByUserName(userName)
                Exit Do
            End If

            Console.WriteLine("{0}Error usuario o password inválidos{0}", Environment.NewLine)

            attempts += 1

        Loop Until attempts = 3
    End Sub

    Private Function AskForUserName() As String
        Console.Write("Ingrese usuario: ")
        Return Console.ReadLine()
    End Function

    Private Function AskForPassword() As String
        Console.Write("Ingrese password: ")
        Dim password As New StringBuilder()
        Do
            Dim keyInfo = Console.ReadKey(True)
            If keyInfo.Key = ConsoleKey.Enter Then
                Exit Do
            End If

            If keyInfo.Key = ConsoleKey.Backspace Then
                If password.Length > 0 Then
                    password.Remove(password.Length - 1, 1)
                    Console.Write("{0} {0}", keyInfo.KeyChar)
                End If
            Else
                password.Append(keyInfo.KeyChar)
                Console.Write("*")
            End If
        Loop
        Return password.ToString()
    End Function

    Private Sub ShowWelcome()
        Console.ForegroundColor = ConsoleColor.Cyan
        Console.WriteLine("{1}{1}*** Bienvenido {0}***{1}", _user, Environment.NewLine)
    End Sub

    Private Sub ShowMenu()
        Console.ForegroundColor = ConsoleColor.Green
        Console.WriteLine("Listados")
        Console.WriteLine(New String("-"c, 8))
        Console.ForegroundColor = ConsoleColor.White
        Console.WriteLine()
        Console.WriteLine("A - Listado de Productos")
        Console.WriteLine("B - Listado de Usuarios")
        Console.WriteLine("C - Salir")
        Console.WriteLine()
        Console.Write("Ingrese Opcion: ")

        Dim options = New Dictionary(Of ConsoleKey, Action) From {
            {ConsoleKey.A, AddressOf ListProducts},
            {ConsoleKey.B, AddressOf ListUsers},
            {ConsoleKey.C, Sub() End}
        }

        Dim key As ConsoleKey
        Do
            key = Console.ReadKey(True).Key
        Loop Until options.Keys.Contains(key)

        options(key)()
    End Sub

    Private Sub ListProducts()
        Console.Clear()
        ShowTitle("Listado de Productos")
        Console.WriteLine("{0,-20} {1,-10} {2,10} {3,10} {4, 6}", "Descripcion",
                          "Registrado", "Precio", "Cantidad", "Estado")
        Console.WriteLine(New String("-"c, 60))
        Console.WriteLine()

        Console.ForegroundColor = ConsoleColor.White

        For Each product In ProductService.GetAll()
            Console.WriteLine("{0,-20} {1,-10} {2,10:C} {3,10:N} {4, 6}", product.Name,
                              product.RegistrationDate.ToShortDateString(), product.Price,
                              product.Quantity, If(product.State, "Activo", "Inactivo"))
        Next
        BackToMenu()
    End Sub

    Private Sub ListUsers()
        Console.Clear()
        ShowTitle("Listado de Usuarios")
        Console.WriteLine("{0,-20} {1,-20} {2,-4}", "Nombre", "Apellidos", "Estado")
        Console.WriteLine(New String("-"c, 50))
        Console.WriteLine()

        Console.ForegroundColor = ConsoleColor.White

        For Each user In UserService.GetAll()
            Console.WriteLine("{0,-20} {1,-20} {2,-4}", user.Name, user.LastName,
                              If(user.State, "Activo", "Inactivo"))
        Next
        BackToMenu()
    End Sub

    Private Sub ShowTitle(title As String)
        Console.ForegroundColor = ConsoleColor.Cyan
        Console.WriteLine(title + Environment.NewLine)
        Console.ForegroundColor = ConsoleColor.Green
    End Sub

    Private Sub BackToMenu()
        Console.Write("{0}{0}Presione una tecla para volver al menú...{0}", Environment.NewLine)
        Console.ReadKey(True)
        Console.Clear()
        ShowMenu()
    End Sub

End Module
