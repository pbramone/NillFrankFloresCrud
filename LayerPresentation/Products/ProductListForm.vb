﻿Imports LayerBDs
Imports LayerDO
Imports LayerPresentation.My.Resources

Namespace Products

    Public Class ProductListForm

        Private ReadOnly _productService As New ProductService(New ProductRepository())

        Private Sub ProductListForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            productsGrid.AutoGenerateColumns = False
            LoadProducts()
        End Sub

        Private Sub LoadProducts()
            productsGrid.DataSource = _productService.GetAll().ToList()

            SetControls(productsGrid.RowCount > 0)

            recordsLabel.Text = String.Format(NumeroDeRegistrosEncontrados, productsGrid.RowCount)

        End Sub

        Private Sub SetControls(active As Boolean)
            filterGroupBox.Enabled = active
            emptyLinkLabel.Visible = Not active
        End Sub

        Private Sub FilteringProducts()

            'TODO: ver si se puede mejorar en un futuro refactoreo
            Select Case propertyNameCombo.Text.ToUpper()
                Case "DESCRIPCION"
                     productsGrid.DataSource = _productService.GetAll() _
                        .Where(Function(p) p.Name.ToUpper() _
                        .Contains(filterTextBox.Text.Trim().ToUpper())).ToList()
                Case "FECHA REGISTRO"
                    productsGrid.DataSource = _productService.GetAll() _
                        .Where(Function(p) p.RegistrationDate.ToShortDateString() _
                        .Contains(filterTextBox.Text.Trim())).ToList()
                Case "STOCK"
                    productsGrid.DataSource = _productService.GetAll() _
                        .Where(Function(p) p.Quantity.ToString() _
                        .Contains(filterTextBox.Text.Trim())).ToList()
            End Select

            emptyLinkLabel.Visible = productsGrid.RowCount = 0

        End Sub

        Private Sub filterTextBox_TextChanged(sender As Object, e As EventArgs) Handles filterTextBox.TextChanged
            Filteringproducts()
        End Sub

    End Class

End Namespace