﻿Imports System.ComponentModel
Imports LayerBDs
Imports LayerDO
Imports LayerENT
Imports LayerPresentation.My.Resources

Namespace Users

    Public Class UserUpdateForm

        Private ReadOnly _userService As New UserService(New UserRepository())

        Private Sub LP_UserUpdate_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            usersGrid.AutoGenerateColumns = False
            LoadUsers()
        End Sub
        Public Sub LoadUsers()
            usersGrid.DataSource = _userService.GetAll().ToList()
        End Sub

        Private Sub editButton_Click(sender As Object, e As EventArgs) Handles editButton.Click

            Dim result = MessageBox.Show(DeseaEditarRegistro, Application.CompanyName,
                                         MessageBoxButtons.OKCancel, MessageBoxIcon.Information)

            If result = DialogResult.OK Then ToggleEditionMode()

        End Sub

        Public Sub ToggleEditionMode()
            nameTextBox.Enabled = Not nameTextBox.Enabled
            lastNameTextBox.Enabled = Not lastNameTextBox.Enabled
            userNameTextBox.Enabled = Not userNameTextBox.Enabled
            passwordTextBox.Enabled = Not passwordTextBox.Enabled
            editAcceptButton.Enabled = Not editAcceptButton.Enabled
            editCancelButton.Enabled = Not editCancelButton.Enabled
            editButton.Enabled = Not editButton.Enabled
            usersGrid.Enabled = Not usersGrid.Enabled
        End Sub

        Private Sub editAcceptButton_Click(sender As Object, e As EventArgs) Handles editAcceptButton.Click
            If Not ValidateChildren() Then Return
            Try
                Dim user = DirectCast(usersGrid.SelectedRows(0).DataBoundItem, User)
                user.Name = nameTextBox.Text.Trim()
                user.LastName = lastNameTextBox.Text.Trim()
                user.UserName = userNameTextBox.Text.Trim()
                user.Password = passwordTextBox.Text.Trim()

                _userService.Update(user)

                LoadUsers()
                ToggleEditionMode()

                MsgBox(ActualizadoConExito, MsgBoxStyle.Information, Title:=Application.CompanyName)

            Catch ex As Exception
                MsgBox(NoSePudoActualizar, MsgBoxStyle.Critical, Title:=ErrorTitle)
            End Try
        End Sub

        Private Sub editCancelButton_Click(sender As Object, e As EventArgs) Handles editCancelButton.Click
            ToggleEditionMode()
            ShowItem()
            ValidateChildren()
        End Sub

        Private Sub Required_Validating(sender As Object, e As CancelEventArgs) Handles nameTextBox.Validating, lastNameTextBox.Validating, userNameTextBox.Validating
            Dim control = DirectCast(sender, TextBox)
            If control.Text.Length > 0 Then
                errorProvider.SetError(control, String.Empty)
            Else
                errorProvider.SetError(control, CampoRequerido)
                e.Cancel = True
            End If
        End Sub

        Private Sub usersGrid_SelectionChanged(sender As Object, e As EventArgs) Handles usersGrid.SelectionChanged
            ShowItem()
        End Sub

        Public Sub ShowItem()
            If usersGrid.SelectedCells.Count = 0 Then Return
            userGroupBox.Text = SeleccionUsuarioTitle
            nameTextBox.Text = usersGrid.SelectedCells.Item(1).Value
            lastNameTextBox.Text = usersGrid.SelectedCells.Item(2).Value
            userNameTextBox.Text = usersGrid.SelectedCells.Item(3).Value
        End Sub

    End Class
End Namespace