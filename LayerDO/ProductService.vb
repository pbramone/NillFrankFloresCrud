﻿Imports LayerDO.Abstract.Repositories

Imports LayerENT

Public Class ProductService

    Private ReadOnly _productRepository As IProductRepository

    Public Sub New(productRepository As IProductRepository)
        _productRepository = productRepository
    End Sub

    Public Function GetAll() As IEnumerable(Of Product)
       return _productRepository.GetAll()
    End Function


    Public Function GetById(id As Guid) As Product
        Return GetAll().FirstOrDefault(Function(product) product.Id = id)
    End Function

    Public Sub AddNew(entity As Product) 
        _productRepository.Insert(entity)
    End Sub

    Public Sub Update(entity As Product) 
        _productRepository.Update(entity)
    End Sub

    ''' <summary>
    ''' Registra una salida de stock de un producto en el almacen
    ''' </summary>
    ''' <param name="entity">Producto</param>
    ''' <param name="discount">Cantidad a descontar</param>
    Public Sub OutputProducts(entity As Product, discount As Double)  
        If discount < 0 OrElse discount > entity.Quantity
            entity.Quantity = 0
        Else 
            entity.Quantity -= discount
        End If   
        _productRepository.Update(entity)
    End Sub

    ''' <summary>
    ''' Registra un ingreso de Stock de un producto en el almacen
    ''' </summary>
    ''' <param name="entity">Producto</param>
    Public Sub EntryProducts(entity As Product, increment As Double)
        If increment > 0
            entity.Quantity += increment
        End If
        _productRepository.Update(entity)
    End Sub

    ''' <summary>
    ''' Marca como inactivo a un Producto
    ''' </summary>
    ''' <param name="entity">Producto</param>
    Public Sub InactiveProduct(entity As Product)
        entity.State = False
        _productRepository.Delete(entity.Id)
    End Sub

End Class