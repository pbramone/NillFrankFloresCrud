﻿Imports System.ComponentModel
Imports LayerBDs
Imports LayerDO
Imports LayerENT
Imports LayerPresentation.My.Resources

Namespace Stock

    Public Class DiscountStockForm

        Private ReadOnly _productService As New ProductService(New ProductRepository())

        Private Sub DiscountStockForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            productsGrid.AutoGenerateColumns = False
            LoadProducts()
        End Sub
        Public Sub LoadProducts()
            productsGrid.DataSource = _productService.GetAll().ToList()
            emptyLinkLabel.Visible = productsGrid.RowCount = 0
        End Sub

        Private Sub productsGrid_SelectionChanged(sender As Object, e As EventArgs) Handles productsGrid.SelectionChanged
            ShowItem()
        End Sub

        Public Sub ShowItem()
            If productsGrid.SelectedCells.Count = 0 Then Return
            productGroupBox.Text = SeleccionProductoTitle
            descriptionTextBox.Text = productsGrid.SelectedCells.Item(0).Value
            priceTextBox.Text = productsGrid.SelectedCells.Item(2).Value
            quantityTextBox.Text = productsGrid.SelectedCells.Item(3).Value
        End Sub

        Private Sub Required_Validating(sender As Object, e As CancelEventArgs) Handles discountTextBox.Validating
            Dim control = DirectCast(sender, Control)
            If control.Text.Length > 0 Then
                errorProvider.SetError(control, String.Empty)
            Else
                errorProvider.SetError(control, CampoRequerido)
                e.Cancel = True
            End If
        End Sub

        Private Sub Number_Validating(sender As Object, e As CancelEventArgs) Handles discountTextBox.Validating
            Dim control = DirectCast(sender, Control)
            If IsNumeric(control.Text) Then
                errorProvider.SetError(control, String.Empty)
            Else
                errorProvider.SetError(control, ErrorNumeroRequerido)
                e.Cancel = True
            End If
        End Sub

        Private Sub outProductButton_Click(sender As Object, e As EventArgs) Handles outProductButton.Click
            If Not ValidateChildren() Then Return
            Try

                Dim product = DirectCast(productsGrid.SelectedRows(0).DataBoundItem, Product)
                Dim discount = Double.Parse(discountTextBox.Text.Trim())

                _productService.OutputProducts(product, discount)

                LoadProducts()

                MsgBox(ActualizadoConExito, MsgBoxStyle.Information, Title:=Application.CompanyName)

            Catch ex As Exception
                MsgBox(NoSePudoActualizar, MsgBoxStyle.Critical, Title:=ErrorTitle)
            End Try
        End Sub

        Private Sub outCancelButton_Click(sender As Object, e As EventArgs) Handles outCancelButton.Click
            Close()
        End Sub

    End Class
End Namespace