﻿Imports LayerENT

Namespace Abstract.Repositories

    Public Interface IUserRepository
        Inherits IRepository(Of User)

        Function ValidateCredentials(userName As String, password As String) As Boolean
        Function GetByUserName(userName As String) As User

    End Interface

End NameSpace