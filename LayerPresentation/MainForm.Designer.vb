﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.tsddb_products = New System.Windows.Forms.ToolStripDropDownButton()
        Me.tsmi_listProducts = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_NewProduct = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_UpdateProduct = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_deletProduct = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_intoProducstStock = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_outProductsStock = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_menu = New System.Windows.Forms.ToolStrip()
        Me.tsddb_Users = New System.Windows.Forms.ToolStripDropDownButton()
        Me.tsmi_ListUser = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_addUsers = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_UpdateUsers = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_deleteUser = New System.Windows.Forms.ToolStripMenuItem()
        Me.st_info = New System.Windows.Forms.StatusStrip()
        Me.userFullNameStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.userNameStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ts_menu.SuspendLayout
        Me.st_info.SuspendLayout
        Me.SuspendLayout
        '
        'tsddb_products
        '
        Me.tsddb_products.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmi_listProducts, Me.tsmi_NewProduct, Me.tsmi_UpdateProduct, Me.tsmi_deletProduct, Me.tsmi_intoProducstStock, Me.tsmi_outProductsStock})
        Me.tsddb_products.Image = CType(resources.GetObject("tsddb_products.Image"),System.Drawing.Image)
        Me.tsddb_products.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsddb_products.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsddb_products.Name = "tsddb_products"
        Me.tsddb_products.Size = New System.Drawing.Size(74, 67)
        Me.tsddb_products.Text = "Productos"
        Me.tsddb_products.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsmi_listProducts
        '
        Me.tsmi_listProducts.Name = "tsmi_listProducts"
        Me.tsmi_listProducts.Size = New System.Drawing.Size(161, 22)
        Me.tsmi_listProducts.Text = "Listar"
        '
        'tsmi_NewProduct
        '
        Me.tsmi_NewProduct.Name = "tsmi_NewProduct"
        Me.tsmi_NewProduct.Size = New System.Drawing.Size(161, 22)
        Me.tsmi_NewProduct.Text = "Agregar"
        '
        'tsmi_UpdateProduct
        '
        Me.tsmi_UpdateProduct.Name = "tsmi_UpdateProduct"
        Me.tsmi_UpdateProduct.Size = New System.Drawing.Size(161, 22)
        Me.tsmi_UpdateProduct.Text = "Editar"
        '
        'tsmi_deletProduct
        '
        Me.tsmi_deletProduct.Name = "tsmi_deletProduct"
        Me.tsmi_deletProduct.Size = New System.Drawing.Size(161, 22)
        Me.tsmi_deletProduct.Text = "Eliminar"
        '
        'tsmi_intoProducstStock
        '
        Me.tsmi_intoProducstStock.Name = "tsmi_intoProducstStock"
        Me.tsmi_intoProducstStock.Size = New System.Drawing.Size(161, 22)
        Me.tsmi_intoProducstStock.Text = "Ingreso de Stock"
        '
        'tsmi_outProductsStock
        '
        Me.tsmi_outProductsStock.Name = "tsmi_outProductsStock"
        Me.tsmi_outProductsStock.Size = New System.Drawing.Size(161, 22)
        Me.tsmi_outProductsStock.Text = "Salida de Stock"
        '
        'ts_menu
        '
        Me.ts_menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsddb_products, Me.tsddb_Users})
        Me.ts_menu.Location = New System.Drawing.Point(0, 0)
        Me.ts_menu.Name = "ts_menu"
        Me.ts_menu.Size = New System.Drawing.Size(865, 70)
        Me.ts_menu.TabIndex = 0
        Me.ts_menu.Text = "ToolStrip1"
        '
        'tsddb_Users
        '
        Me.tsddb_Users.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmi_ListUser, Me.tsmi_addUsers, Me.tsmi_UpdateUsers, Me.tsmi_deleteUser})
        Me.tsddb_Users.Image = CType(resources.GetObject("tsddb_Users.Image"),System.Drawing.Image)
        Me.tsddb_Users.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsddb_Users.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsddb_Users.Name = "tsddb_Users"
        Me.tsddb_Users.Size = New System.Drawing.Size(65, 67)
        Me.tsddb_Users.Text = "Usuarios"
        Me.tsddb_Users.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsmi_ListUser
        '
        Me.tsmi_ListUser.Name = "tsmi_ListUser"
        Me.tsmi_ListUser.Size = New System.Drawing.Size(152, 22)
        Me.tsmi_ListUser.Text = "Listar"
        '
        'tsmi_addUsers
        '
        Me.tsmi_addUsers.Name = "tsmi_addUsers"
        Me.tsmi_addUsers.Size = New System.Drawing.Size(152, 22)
        Me.tsmi_addUsers.Text = "Agregar"
        '
        'tsmi_UpdateUsers
        '
        Me.tsmi_UpdateUsers.Name = "tsmi_UpdateUsers"
        Me.tsmi_UpdateUsers.Size = New System.Drawing.Size(152, 22)
        Me.tsmi_UpdateUsers.Text = "Editar"
        '
        'tsmi_deleteUser
        '
        Me.tsmi_deleteUser.Name = "tsmi_deleteUser"
        Me.tsmi_deleteUser.Size = New System.Drawing.Size(152, 22)
        Me.tsmi_deleteUser.Text = "Eliminar"
        '
        'st_info
        '
        Me.st_info.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.userFullNameStatusLabel, Me.ToolStripProgressBar1, Me.userNameStatusLabel})
        Me.st_info.Location = New System.Drawing.Point(0, 438)
        Me.st_info.Name = "st_info"
        Me.st_info.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.st_info.Size = New System.Drawing.Size(865, 22)
        Me.st_info.TabIndex = 2
        Me.st_info.Text = "StatusStrip1"
        '
        'tssl_user
        '
        Me.userFullNameStatusLabel.Name = "tssl_user"
        Me.userFullNameStatusLabel.Size = New System.Drawing.Size(10, 17)
        Me.userFullNameStatusLabel.Text = "I"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(100, 16)
        '
        'tssl_uName
        '
        Me.userNameStatusLabel.Name = "tssl_uName"
        Me.userNameStatusLabel.Size = New System.Drawing.Size(10, 17)
        Me.userNameStatusLabel.Text = "I"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(865, 460)
        Me.Controls.Add(Me.st_info)
        Me.Controls.Add(Me.ts_menu)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.IsMdiContainer = true
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu Demo de Operaciones CRUD en N-Capas Full POO and T-SQL By DevSoft Nill Frank"& _ 
    " Flores :-)"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ts_menu.ResumeLayout(false)
        Me.ts_menu.PerformLayout
        Me.st_info.ResumeLayout(false)
        Me.st_info.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents tsddb_products As ToolStripDropDownButton
    Friend WithEvents tsmi_NewProduct As ToolStripMenuItem
    Friend WithEvents tsmi_UpdateProduct As ToolStripMenuItem
    Friend WithEvents tsmi_deletProduct As ToolStripMenuItem
    Friend WithEvents tsmi_listProducts As ToolStripMenuItem
    Friend WithEvents ts_menu As ToolStrip
    Friend WithEvents tsddb_Users As ToolStripDropDownButton
    Friend WithEvents tsmi_ListUser As ToolStripMenuItem
    Friend WithEvents tsmi_addUsers As ToolStripMenuItem
    Friend WithEvents tsmi_UpdateUsers As ToolStripMenuItem
    Friend WithEvents tsmi_deleteUser As ToolStripMenuItem
    Friend WithEvents st_info As StatusStrip
    Friend WithEvents userFullNameStatusLabel As ToolStripStatusLabel
    Friend WithEvents ToolStripProgressBar1 As ToolStripProgressBar
    Friend WithEvents userNameStatusLabel As ToolStripStatusLabel
    Friend WithEvents tsmi_intoProducstStock As ToolStripMenuItem
    Friend WithEvents tsmi_outProductsStock As ToolStripMenuItem
End Class
