﻿Imports System.ComponentModel
Imports LayerBDs
Imports LayerDO
Imports LayerENT
Imports LayerPresentation.My.Resources

Namespace Users

    Public Class UserCreateForm

        Private Sub UserCreateForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load       
            SetNewId()
        End Sub

        Sub SetNewId()
            idTextBox.Text = Guid.NewGuid().ToString()
        End Sub

        Private Sub Required_Validating(sender As Object, e As CancelEventArgs) Handles nameTextBox.Validating, lastNameTextBox.Validating, userNameTextBox.Validating, passwordTextBox.Validating
            Dim control = DirectCast(sender, TextBox)
            If control.Text.Length > 0 Then
                errorProvider.SetError(control, String.Empty)
            Else
                errorProvider.SetError(control, CampoRequerido)
                e.Cancel = True
            End If
        End Sub

        Private Sub insertButton_Click(sender As Object, e As EventArgs) Handles insertButton.Click
            If Not ValidateChildren() Then Return

            Dim userService As New UserService(New UserRepository())

            Dim user As New User With {
                .Id = Guid.Parse(idTextBox.Text),
                .Name = nameTextBox.Text.Trim(),
                .LastName = lastNameTextBox.Text.Trim(),
                .UserName = userNameTextBox.Text.Trim(),
                .Password = passwordTextBox.Text.Trim(),
                .State = stateCheckBox.Checked
            }

            Try
                userService.AddNew(user)
                ClearControls()
                SetNewId()
                MsgBox(GuardadoConExito, MsgBoxStyle.Information, Title:=Application.CompanyName)

            Catch ex As Exception
                MsgBox(NoSePudoGuardar, MsgBoxStyle.Critical, Title:=ErrorTitle)
            End Try

        End Sub
        Private Sub ClearControls()
            idTextBox.Clear()
            nameTextBox.Clear()
            lastNameTextBox.Clear()
            userNameTextBox.Clear()
            passwordTextBox.Clear()
            hashTextBox.Clear()
        End Sub

        Private Sub insertCancelButton_Click(sender As Object, e As EventArgs) Handles insertCancelButton.Click
            Close()
        End Sub

    End Class
End Namespace