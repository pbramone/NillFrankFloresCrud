﻿Namespace Users
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class UserCreateForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserCreateForm))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.stateCheckBox = New System.Windows.Forms.CheckBox()
        Me.hashTextBox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.userNameTextBox = New System.Windows.Forms.TextBox()
        Me.passwordTextBox = New System.Windows.Forms.TextBox()
        Me.lastNameTextBox = New System.Windows.Forms.TextBox()
        Me.nameTextBox = New System.Windows.Forms.TextBox()
        Me.idTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.insertButton = New System.Windows.Forms.Button()
        Me.insertCancelButton = New System.Windows.Forms.Button()
        Me.errorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox1.SuspendLayout
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.stateCheckBox)
        Me.GroupBox1.Controls.Add(Me.hashTextBox)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.userNameTextBox)
        Me.GroupBox1.Controls.Add(Me.passwordTextBox)
        Me.GroupBox1.Controls.Add(Me.lastNameTextBox)
        Me.GroupBox1.Controls.Add(Me.nameTextBox)
        Me.GroupBox1.Controls.Add(Me.idTextBox)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(459, 148)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Registro de Usuario"
        '
        'stateCheckBox
        '
        Me.stateCheckBox.AutoSize = true
        Me.stateCheckBox.Checked = true
        Me.stateCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.stateCheckBox.Location = New System.Drawing.Point(377, 32)
        Me.stateCheckBox.Name = "stateCheckBox"
        Me.stateCheckBox.Size = New System.Drawing.Size(59, 20)
        Me.stateCheckBox.TabIndex = 12
        Me.stateCheckBox.Text = "Activo"
        Me.stateCheckBox.UseVisualStyleBackColor = true
        '
        'txt_hash
        '
        Me.hashTextBox.Location = New System.Drawing.Point(224, 111)
        Me.hashTextBox.Name = "hashTextBox"
        Me.hashTextBox.Size = New System.Drawing.Size(212, 20)
        Me.hashTextBox.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Location = New System.Drawing.Point(224, 95)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 16)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Valor HASH"
        '
        'userNameTextBox
        '
        Me.userNameTextBox.Location = New System.Drawing.Point(224, 32)
        Me.userNameTextBox.Name = "userNameTextBox"
        Me.userNameTextBox.Size = New System.Drawing.Size(119, 20)
        Me.userNameTextBox.TabIndex = 9
        '
        'passwordTextBox
        '
        Me.passwordTextBox.Location = New System.Drawing.Point(224, 71)
        Me.passwordTextBox.Name = "passwordTextBox"
        Me.passwordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(36)
        Me.passwordTextBox.Size = New System.Drawing.Size(123, 20)
        Me.passwordTextBox.TabIndex = 8
        '
        'lastNameTextBox
        '
        Me.lastNameTextBox.Location = New System.Drawing.Point(20, 111)
        Me.lastNameTextBox.Name = "lastNameTextBox"
        Me.lastNameTextBox.Size = New System.Drawing.Size(175, 20)
        Me.lastNameTextBox.TabIndex = 7
        '
        'nameTextBox
        '
        Me.nameTextBox.Location = New System.Drawing.Point(20, 71)
        Me.nameTextBox.Name = "nameTextBox"
        Me.nameTextBox.Size = New System.Drawing.Size(175, 20)
        Me.nameTextBox.TabIndex = 6
        '
        'idTextBox
        '
        Me.idTextBox.Enabled = false
        Me.idTextBox.Location = New System.Drawing.Point(20, 32)
        Me.idTextBox.Name = "idTextBox"
        Me.idTextBox.ReadOnly = true
        Me.idTextBox.Size = New System.Drawing.Size(175, 20)
        Me.idTextBox.TabIndex = 5
        Me.idTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Location = New System.Drawing.Point(224, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "User Pass"
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(224, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "User Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(20, 95)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Apellidos"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(20, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nombres"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(20, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Codigo"
        '
        'insertButton
        '
        Me.insertButton.Image = CType(resources.GetObject("insertButton.Image"),System.Drawing.Image)
        Me.insertButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.insertButton.Location = New System.Drawing.Point(110, 159)
        Me.insertButton.Name = "insertButton"
        Me.insertButton.Size = New System.Drawing.Size(120, 57)
        Me.insertButton.TabIndex = 1
        Me.insertButton.Text = "Registrar"
        Me.insertButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.insertButton.UseVisualStyleBackColor = true
        '
        'insertCancelButton
        '
        Me.insertCancelButton.Image = CType(resources.GetObject("insertCancelButton.Image"),System.Drawing.Image)
        Me.insertCancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.insertCancelButton.Location = New System.Drawing.Point(238, 159)
        Me.insertCancelButton.Name = "insertCancelButton"
        Me.insertCancelButton.Size = New System.Drawing.Size(120, 57)
        Me.insertCancelButton.TabIndex = 2
        Me.insertCancelButton.Text = "Cancelar"
        Me.insertCancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.insertCancelButton.UseVisualStyleBackColor = true
        '
        'errorProvider
        '
        Me.errorProvider.ContainerControl = Me
        '
        'UserCreateForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.ClientSize = New System.Drawing.Size(469, 220)
        Me.Controls.Add(Me.insertCancelButton)
        Me.Controls.Add(Me.insertButton)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "UserCreateForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Crear Usuarios"
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

        Friend WithEvents GroupBox1 As GroupBox
        Friend WithEvents Label5 As Label
        Friend WithEvents Label4 As Label
        Friend WithEvents Label3 As Label
        Friend WithEvents Label2 As Label
        Friend WithEvents Label1 As Label
        Friend WithEvents userNameTextBox As TextBox
        Friend WithEvents passwordTextBox As TextBox
        Friend WithEvents lastNameTextBox As TextBox
        Friend WithEvents nameTextBox As TextBox
        Friend WithEvents idTextBox As TextBox
        Friend WithEvents hashTextBox As TextBox
        Friend WithEvents Label6 As Label
        Friend WithEvents insertButton As Button
        Friend WithEvents insertCancelButton As Button
        Friend WithEvents errorProvider As ErrorProvider
        Friend WithEvents stateCheckBox As CheckBox
    End Class
End NameSpace