﻿Imports System.ComponentModel
Imports LayerBDs
Imports LayerDO
Imports LayerENT
Imports LayerPresentation.My.Resources

Namespace Products

    Public Class ProductCreateForm
        Private Sub ProductCreateForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            SetNewId()
        End Sub

        Sub SetNewId()
            idTextBox.Text = Guid.NewGuid().ToString()
            dateLabel.Text = Date.Now.ToString("dd/MM/yyyy HH:mm")
        End Sub

        Private Sub Required_Validating(sender As Object, e As CancelEventArgs) Handles quantityTextBox.Validating, priceTextBox.Validating, descriptionTextBox.Validating
            Dim control = DirectCast(sender, Control)
            If control.Text.Length > 0 Then
                errorProvider.SetError(control, String.Empty)
            Else
                errorProvider.SetError(control, CampoRequerido)
                e.Cancel = True
            End If
        End Sub

        Private Sub Number_Validating(sender As Object, e As CancelEventArgs) Handles  quantityTextBox.Validating, priceTextBox.Validating
            Dim control = DirectCast(sender, Control)
            If IsNumeric(control.Text)
                errorProvider.SetError(control, String.Empty)
            Else 
                errorProvider.SetError(control, ErrorNumeroRequerido)
                e.Cancel = True
            End If
        End Sub

        Private Sub insertButton_Click(sender As Object, e As EventArgs) Handles insertButton.Click
            If Not ValidateChildren() Then Return
            Try
                Dim productService As New ProductService(New ProductRepository())

                Dim product As New Product With {
                    .Id = Guid.Parse(idTextBox.Text),
                    .Name = descriptionTextBox.Text.Trim(),
                    .RegistrationDate = dateLabel.Text.Trim(),
                    .Price = priceTextBox.Text.Trim(),
                    .Quantity = quantityTextBox.Text.Trim(),
                    .State = stateChackBox.Checked
                }

                productService.AddNew(product)

                ClearControls()
                SetNewId()
                MsgBox(GuardadoConExito, MsgBoxStyle.Information, Title:=Application.CompanyName)

            Catch ex As Exception
                MsgBox(NoSePudoGuardar, MsgBoxStyle.Critical, Title:=ErrorTitle)
            End Try

        End Sub

        Private Sub ClearControls()
            idTextBox.Clear()
            descriptionTextBox.Clear()
            priceTextBox.Clear()
            quantityTextBox.Clear()
        End Sub

        Private Sub insertCancelButton_Click(sender As Object, e As EventArgs) Handles insertCancelButton.Click
            Close()
        End Sub

    End Class
End Namespace