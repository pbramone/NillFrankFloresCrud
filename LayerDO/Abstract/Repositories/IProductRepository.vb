﻿Imports LayerENT

Namespace Abstract.Repositories

    Public Interface IProductRepository
        Inherits IRepository(Of Product)

    End Interface

End NameSpace