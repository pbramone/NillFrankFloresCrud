﻿Namespace Users
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class UserUpdateForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserUpdateForm))
        Me.userGroupBox = New System.Windows.Forms.GroupBox()
        Me.passwordTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.userNameTextBox = New System.Windows.Forms.TextBox()
        Me.lastNameTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nameTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.usersGrid = New System.Windows.Forms.DataGridView()
        Me.ColumnUserName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserLastName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserUserName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserState = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.editCancelButton = New System.Windows.Forms.Button()
        Me.editAcceptButton = New System.Windows.Forms.Button()
        Me.editButton = New System.Windows.Forms.Button()
        Me.errorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.userGroupBox.SuspendLayout
        CType(Me.usersGrid,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'userGroupBox
        '
        Me.userGroupBox.Controls.Add(Me.passwordTextBox)
        Me.userGroupBox.Controls.Add(Me.Label4)
        Me.userGroupBox.Controls.Add(Me.userNameTextBox)
        Me.userGroupBox.Controls.Add(Me.lastNameTextBox)
        Me.userGroupBox.Controls.Add(Me.Label3)
        Me.userGroupBox.Controls.Add(Me.Label2)
        Me.userGroupBox.Controls.Add(Me.nameTextBox)
        Me.userGroupBox.Controls.Add(Me.Label1)
        Me.userGroupBox.Location = New System.Drawing.Point(29, 152)
        Me.userGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.userGroupBox.Name = "userGroupBox"
        Me.userGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.userGroupBox.Size = New System.Drawing.Size(406, 115)
        Me.userGroupBox.TabIndex = 12
        Me.userGroupBox.TabStop = false
        '
        'passwordTextBox
        '
        Me.passwordTextBox.Enabled = false
        Me.passwordTextBox.Location = New System.Drawing.Point(215, 72)
        Me.passwordTextBox.Name = "passwordTextBox"
        Me.passwordTextBox.Size = New System.Drawing.Size(176, 20)
        Me.passwordTextBox.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(215, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Clave de Acceso"
        '
        'userNameTextBox
        '
        Me.userNameTextBox.Enabled = false
        Me.userNameTextBox.Location = New System.Drawing.Point(15, 72)
        Me.userNameTextBox.Name = "userNameTextBox"
        Me.userNameTextBox.Size = New System.Drawing.Size(176, 20)
        Me.userNameTextBox.TabIndex = 7
        '
        'lastNameTextBox
        '
        Me.lastNameTextBox.Enabled = false
        Me.lastNameTextBox.Location = New System.Drawing.Point(215, 33)
        Me.lastNameTextBox.Name = "lastNameTextBox"
        Me.lastNameTextBox.Size = New System.Drawing.Size(176, 20)
        Me.lastNameTextBox.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(15, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Nombre de Usuario"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(215, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Apellidos"
        '
        'nameTextBox
        '
        Me.nameTextBox.Enabled = false
        Me.nameTextBox.Location = New System.Drawing.Point(15, 33)
        Me.nameTextBox.Name = "nameTextBox"
        Me.nameTextBox.Size = New System.Drawing.Size(176, 20)
        Me.nameTextBox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(15, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombres"
        '
        'usersGrid
        '
        Me.usersGrid.AllowUserToAddRows = false
        Me.usersGrid.AllowUserToDeleteRows = false
        Me.usersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.usersGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnUserName, Me.ColumnUserLastName, Me.ColumnUserUserName, Me.ColumnUserState})
        Me.usersGrid.Location = New System.Drawing.Point(4, 10)
        Me.usersGrid.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.usersGrid.Name = "usersGrid"
        Me.usersGrid.ReadOnly = true
        Me.usersGrid.RowHeadersVisible = false
        Me.usersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.usersGrid.Size = New System.Drawing.Size(457, 134)
        Me.usersGrid.TabIndex = 11
        '
        'ColumnUserName
        '
        Me.ColumnUserName.DataPropertyName = "Name"
        Me.ColumnUserName.HeaderText = "Nombres"
        Me.ColumnUserName.Name = "ColumnUserName"
        Me.ColumnUserName.ReadOnly = true
        Me.ColumnUserName.Width = 150
        '
        'ColumnUserLastName
        '
        Me.ColumnUserLastName.DataPropertyName = "LastName"
        Me.ColumnUserLastName.HeaderText = "Apellidos"
        Me.ColumnUserLastName.Name = "ColumnUserLastName"
        Me.ColumnUserLastName.ReadOnly = true
        Me.ColumnUserLastName.Width = 150
        '
        'ColumnUserUserName
        '
        Me.ColumnUserUserName.DataPropertyName = "UserName"
        Me.ColumnUserUserName.HeaderText = "NombreUsuario"
        Me.ColumnUserUserName.Name = "ColumnUserUserName"
        Me.ColumnUserUserName.ReadOnly = true
        Me.ColumnUserUserName.Width = 95
        '
        'ColumnUserState
        '
        Me.ColumnUserState.DataPropertyName = "State"
        Me.ColumnUserState.HeaderText = "Activo"
        Me.ColumnUserState.Name = "ColumnUserState"
        Me.ColumnUserState.ReadOnly = true
        Me.ColumnUserState.Width = 50
        '
        'editCancelButton
        '
        Me.editCancelButton.Enabled = false
        Me.editCancelButton.Image = CType(resources.GetObject("editCancelButton.Image"),System.Drawing.Image)
        Me.editCancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.editCancelButton.Location = New System.Drawing.Point(300, 273)
        Me.editCancelButton.Name = "editCancelButton"
        Me.editCancelButton.Size = New System.Drawing.Size(120, 58)
        Me.editCancelButton.TabIndex = 15
        Me.editCancelButton.Text = "Cancelar"
        Me.editCancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.editCancelButton.UseVisualStyleBackColor = true
        '
        'editAcceptButton
        '
        Me.editAcceptButton.Enabled = false
        Me.editAcceptButton.Image = CType(resources.GetObject("editAcceptButton.Image"),System.Drawing.Image)
        Me.editAcceptButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.editAcceptButton.Location = New System.Drawing.Point(172, 273)
        Me.editAcceptButton.Name = "editAcceptButton"
        Me.editAcceptButton.Size = New System.Drawing.Size(120, 58)
        Me.editAcceptButton.TabIndex = 14
        Me.editAcceptButton.Text = "Aceptar"
        Me.editAcceptButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.editAcceptButton.UseVisualStyleBackColor = true
        '
        'editButton
        '
        Me.editButton.Image = CType(resources.GetObject("editButton.Image"),System.Drawing.Image)
        Me.editButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.editButton.Location = New System.Drawing.Point(44, 273)
        Me.editButton.Name = "editButton"
        Me.editButton.Size = New System.Drawing.Size(120, 58)
        Me.editButton.TabIndex = 13
        Me.editButton.Text = "Editar"
        Me.editButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.editButton.UseVisualStyleBackColor = true
        '
        'ep_errores
        '
        Me.errorProvider.ContainerControl = Me
        '
        'UserUpdateForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.ClientSize = New System.Drawing.Size(465, 345)
        Me.Controls.Add(Me.userGroupBox)
        Me.Controls.Add(Me.usersGrid)
        Me.Controls.Add(Me.editCancelButton)
        Me.Controls.Add(Me.editAcceptButton)
        Me.Controls.Add(Me.editButton)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "UserUpdateForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Actualizar Usuarios"
        Me.userGroupBox.ResumeLayout(false)
        Me.userGroupBox.PerformLayout
        CType(Me.usersGrid,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

        Friend WithEvents userGroupBox As GroupBox
        Friend WithEvents userNameTextBox As TextBox
        Friend WithEvents lastNameTextBox As TextBox
        Friend WithEvents Label3 As Label
        Friend WithEvents Label2 As Label
        Friend WithEvents nameTextBox As TextBox
        Friend WithEvents Label1 As Label
        Friend WithEvents usersGrid As DataGridView
        Friend WithEvents editCancelButton As Button
        Friend WithEvents editAcceptButton As Button
        Friend WithEvents editButton As Button
        Friend WithEvents errorProvider As ErrorProvider
        Friend WithEvents passwordTextBox As TextBox
        Friend WithEvents Label4 As Label
        Friend WithEvents ColumnUserName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserLastName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserUserName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserState As DataGridViewCheckBoxColumn
    End Class
End NameSpace