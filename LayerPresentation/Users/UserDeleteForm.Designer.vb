﻿Namespace Users
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class UserDeleteForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserDeleteForm))
        Me.usersGrid = New System.Windows.Forms.DataGridView()
        Me.ColumnUserName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserLastName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserUserName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserState = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.deleteCancelButton = New System.Windows.Forms.Button()
        Me.deleteAcceptButton = New System.Windows.Forms.Button()
        Me.deleteButton = New System.Windows.Forms.Button()
        Me.userGroupBox = New System.Windows.Forms.GroupBox()
        Me.userNameTextBox = New System.Windows.Forms.TextBox()
        Me.lastNameTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nameTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.usersGrid,System.ComponentModel.ISupportInitialize).BeginInit
        Me.userGroupBox.SuspendLayout
        Me.SuspendLayout
        '
        'usersGrid
        '
        Me.usersGrid.AllowUserToAddRows = false
        Me.usersGrid.AllowUserToDeleteRows = false
        Me.usersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.usersGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnUserName, Me.ColumnUserLastName, Me.ColumnUserUserName, Me.ColumnUserState})
        Me.usersGrid.Location = New System.Drawing.Point(3, 3)
        Me.usersGrid.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.usersGrid.Name = "usersGrid"
        Me.usersGrid.ReadOnly = true
        Me.usersGrid.RowHeadersVisible = false
        Me.usersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.usersGrid.Size = New System.Drawing.Size(517, 134)
        Me.usersGrid.TabIndex = 14
        '
        'ColumnUserName
        '
        Me.ColumnUserName.DataPropertyName = "Name"
        Me.ColumnUserName.HeaderText = "Nombres"
        Me.ColumnUserName.Name = "ColumnUserName"
        Me.ColumnUserName.ReadOnly = true
        Me.ColumnUserName.Width = 150
        '
        'ColumnUserLastName
        '
        Me.ColumnUserLastName.DataPropertyName = "LastName"
        Me.ColumnUserLastName.HeaderText = "Apellidos"
        Me.ColumnUserLastName.Name = "ColumnUserLastName"
        Me.ColumnUserLastName.ReadOnly = true
        Me.ColumnUserLastName.Width = 150
        '
        'ColumnUserUserName
        '
        Me.ColumnUserUserName.DataPropertyName = "UserName"
        Me.ColumnUserUserName.HeaderText = "NombreUsuario"
        Me.ColumnUserUserName.Name = "ColumnUserUserName"
        Me.ColumnUserUserName.ReadOnly = true
        Me.ColumnUserUserName.Width = 95
        '
        'ColumnUserState
        '
        Me.ColumnUserState.DataPropertyName = "State"
        Me.ColumnUserState.HeaderText = "Activo"
        Me.ColumnUserState.Name = "ColumnUserState"
        Me.ColumnUserState.ReadOnly = true
        Me.ColumnUserState.Width = 50
        '
        'deleteCancelButton
        '
        Me.deleteCancelButton.Enabled = false
        Me.deleteCancelButton.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.deleteCancelButton.Image = CType(resources.GetObject("deleteCancelButton.Image"),System.Drawing.Image)
        Me.deleteCancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.deleteCancelButton.Location = New System.Drawing.Point(330, 267)
        Me.deleteCancelButton.Name = "deleteCancelButton"
        Me.deleteCancelButton.Size = New System.Drawing.Size(120, 62)
        Me.deleteCancelButton.TabIndex = 18
        Me.deleteCancelButton.Text = "Cancelar"
        Me.deleteCancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.deleteCancelButton.UseVisualStyleBackColor = true
        '
        'deleteAcceptButton
        '
        Me.deleteAcceptButton.Enabled = false
        Me.deleteAcceptButton.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.deleteAcceptButton.Image = CType(resources.GetObject("deleteAcceptButton.Image"),System.Drawing.Image)
        Me.deleteAcceptButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.deleteAcceptButton.Location = New System.Drawing.Point(202, 267)
        Me.deleteAcceptButton.Name = "deleteAcceptButton"
        Me.deleteAcceptButton.Size = New System.Drawing.Size(120, 62)
        Me.deleteAcceptButton.TabIndex = 17
        Me.deleteAcceptButton.Text = "Aceptar"
        Me.deleteAcceptButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.deleteAcceptButton.UseVisualStyleBackColor = true
        '
        'deleteButton
        '
        Me.deleteButton.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.deleteButton.Image = CType(resources.GetObject("deleteButton.Image"),System.Drawing.Image)
        Me.deleteButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.deleteButton.Location = New System.Drawing.Point(74, 267)
        Me.deleteButton.Name = "deleteButton"
        Me.deleteButton.Size = New System.Drawing.Size(120, 62)
        Me.deleteButton.TabIndex = 16
        Me.deleteButton.Text = "Eliminar"
        Me.deleteButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.deleteButton.UseVisualStyleBackColor = true
        '
        'userGroupBox
        '
        Me.userGroupBox.Controls.Add(Me.userNameTextBox)
        Me.userGroupBox.Controls.Add(Me.lastNameTextBox)
        Me.userGroupBox.Controls.Add(Me.Label3)
        Me.userGroupBox.Controls.Add(Me.Label2)
        Me.userGroupBox.Controls.Add(Me.nameTextBox)
        Me.userGroupBox.Controls.Add(Me.Label1)
        Me.userGroupBox.Location = New System.Drawing.Point(77, 145)
        Me.userGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.userGroupBox.Name = "userGroupBox"
        Me.userGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.userGroupBox.Size = New System.Drawing.Size(370, 115)
        Me.userGroupBox.TabIndex = 19
        Me.userGroupBox.TabStop = false
        '
        'userNameTextBox
        '
        Me.userNameTextBox.Enabled = false
        Me.userNameTextBox.Location = New System.Drawing.Point(6, 72)
        Me.userNameTextBox.Name = "userNameTextBox"
        Me.userNameTextBox.Size = New System.Drawing.Size(176, 20)
        Me.userNameTextBox.TabIndex = 7
        '
        'lastNameTextBox
        '
        Me.lastNameTextBox.Enabled = false
        Me.lastNameTextBox.Location = New System.Drawing.Point(187, 33)
        Me.lastNameTextBox.Name = "lastNameTextBox"
        Me.lastNameTextBox.Size = New System.Drawing.Size(176, 20)
        Me.lastNameTextBox.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(6, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Nombre de Usuario"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(187, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Apellidos"
        '
        'nameTextBox
        '
        Me.nameTextBox.Enabled = false
        Me.nameTextBox.Location = New System.Drawing.Point(6, 33)
        Me.nameTextBox.Name = "nameTextBox"
        Me.nameTextBox.Size = New System.Drawing.Size(176, 20)
        Me.nameTextBox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(6, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombres"
        '
        'UserDeleteForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 336)
        Me.Controls.Add(Me.userGroupBox)
        Me.Controls.Add(Me.usersGrid)
        Me.Controls.Add(Me.deleteCancelButton)
        Me.Controls.Add(Me.deleteAcceptButton)
        Me.Controls.Add(Me.deleteButton)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "UserDeleteForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Eliminar Usuarios"
        CType(Me.usersGrid,System.ComponentModel.ISupportInitialize).EndInit
        Me.userGroupBox.ResumeLayout(false)
        Me.userGroupBox.PerformLayout
        Me.ResumeLayout(false)

End Sub

        Friend WithEvents usersGrid As DataGridView
        Friend WithEvents deleteCancelButton As Button
        Friend WithEvents deleteAcceptButton As Button
        Friend WithEvents deleteButton As Button
        Friend WithEvents userGroupBox As GroupBox
        Friend WithEvents userNameTextBox As TextBox
        Friend WithEvents lastNameTextBox As TextBox
        Friend WithEvents Label3 As Label
        Friend WithEvents Label2 As Label
        Friend WithEvents nameTextBox As TextBox
        Friend WithEvents Label1 As Label
        Friend WithEvents ColumnUserName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserLastName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserUserName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserState As DataGridViewCheckBoxColumn
    End Class
End NameSpace