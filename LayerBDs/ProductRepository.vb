﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports LayerDO.Abstract.Repositories

Imports LayerENT
Public Class ProductRepository
    Implements IProductRepository

    Private ReadOnly _connString As String

    Public Sub New()
        _connString = ConfigurationManager.ConnectionStrings("Fernando").ConnectionString
    End Sub

    Public Sub New(connString As String)
        _connString = connString
    End Sub

    Public Function GetAll() As IEnumerable(Of Product) Implements IProductRepository.GetAll
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("ListProducts")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn

            Using reader = cmd.ExecuteReader()
                Dim products = New List(Of Product)
                While reader.Read()
                    Dim product = New Product With {
                        .Id = reader.GetGuid(0),
                        .Name = reader.GetString(1),
                        .RegistrationDate = reader.GetDateTime(2),
                        .Price = reader.GetDecimal(3),
                        .Quantity = If(reader.IsDBNull(4), 0, reader.GetDecimal(4)),
                        .State = (Not reader.IsDBNull(5) AndAlso reader.GetBoolean(5))
                    }
                    products.Add(product)
                End While
                Return products
            End Using
        End Using
    End Function

    Public Sub Insert(entity As Product) Implements IRepository(Of Product).Insert
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("AddNewProduct")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn

            cmd.Parameters.AddWithValue("@identificador", entity.Id)
            cmd.Parameters.AddWithValue("@NameProduct", entity.Name)
            cmd.Parameters.AddWithValue("@DateReg", entity.RegistrationDate)
            cmd.Parameters.AddWithValue("@Price", entity.Price())
            cmd.Parameters.AddWithValue("@Quantity", entity.Quantity)
            cmd.Parameters.AddWithValue("@State", entity.State)

            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Public Sub Update(entity As Product) Implements IProductRepository.Update
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("UpdateProducts")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn

            cmd.Parameters.AddWithValue("@identificador", entity.Id)
            cmd.Parameters.AddWithValue("@NameProduct", entity.Name)
            cmd.Parameters.AddWithValue("@Price", entity.Price())
            cmd.Parameters.AddWithValue("@Quantity", entity.Quantity)

            cmd.ExecuteNonQuery()
        End Using
    End Sub

    ''' <summary>
    ''' Marca como inactivo a un Producto
    ''' </summary>
    ''' <param name="id">Identificador unico del Producto</param>
    Public Sub Delete(id As Guid) Implements IRepository(Of Product).Delete
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("DeleteProducts")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn

            cmd.Parameters.AddWithValue("@identificador", id)

            cmd.ExecuteNonQuery()
        End Using
    End Sub

End Class
