﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoginForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoginForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.userNameTextBox = New System.Windows.Forms.TextBox()
        Me.passwordTextBox = New System.Windows.Forms.TextBox()
        Me.loginButton = New System.Windows.Forms.Button()
        Me.loginCancelButton = New System.Windows.Forms.Button()
        Me.timeLabel = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(21, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre de Usuario"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(21, 122)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Contraseña de Acceso"
        '
        'userNameTextBox
        '
        Me.userNameTextBox.Location = New System.Drawing.Point(21, 97)
        Me.userNameTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.userNameTextBox.Name = "userNameTextBox"
        Me.userNameTextBox.Size = New System.Drawing.Size(162, 25)
        Me.userNameTextBox.TabIndex = 2
        Me.userNameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'passwordTextBox
        '
        Me.passwordTextBox.Font = New System.Drawing.Font("Segoe UI", 8!)
        Me.passwordTextBox.Location = New System.Drawing.Point(21, 139)
        Me.passwordTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.passwordTextBox.Name = "passwordTextBox"
        Me.passwordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.passwordTextBox.Size = New System.Drawing.Size(162, 22)
        Me.passwordTextBox.TabIndex = 3
        Me.passwordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'loginButton
        '
        Me.loginButton.Image = CType(resources.GetObject("loginButton.Image"),System.Drawing.Image)
        Me.loginButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.loginButton.Location = New System.Drawing.Point(216, 63)
        Me.loginButton.Name = "loginButton"
        Me.loginButton.Size = New System.Drawing.Size(140, 55)
        Me.loginButton.TabIndex = 4
        Me.loginButton.Text = "&Ingresar"
        Me.loginButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.loginButton.UseVisualStyleBackColor = true
        '
        'loginCancelButton
        '
        Me.loginCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.loginCancelButton.Image = CType(resources.GetObject("loginCancelButton.Image"),System.Drawing.Image)
        Me.loginCancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.loginCancelButton.Location = New System.Drawing.Point(216, 118)
        Me.loginCancelButton.Name = "loginCancelButton"
        Me.loginCancelButton.Size = New System.Drawing.Size(140, 55)
        Me.loginCancelButton.TabIndex = 5
        Me.loginCancelButton.Text = "&Salir"
        Me.loginCancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.loginCancelButton.UseVisualStyleBackColor = true
        '
        'timeLabel
        '
        Me.timeLabel.AutoSize = true
        Me.timeLabel.Font = New System.Drawing.Font("Bauhaus 93", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.timeLabel.Location = New System.Drawing.Point(110, 22)
        Me.timeLabel.Name = "timeLabel"
        Me.timeLabel.Size = New System.Drawing.Size(16, 21)
        Me.timeLabel.TabIndex = 6
        Me.timeLabel.Text = "I"
        '
        'Timer1
        '
        Me.Timer1.Enabled = true
        '
        'LoginForm
        '
        Me.AcceptButton = Me.loginButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 17!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.loginCancelButton
        Me.ClientSize = New System.Drawing.Size(397, 200)
        Me.Controls.Add(Me.timeLabel)
        Me.Controls.Add(Me.loginCancelButton)
        Me.Controls.Add(Me.loginButton)
        Me.Controls.Add(Me.passwordTextBox)
        Me.Controls.Add(Me.userNameTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Segoe UI", 10!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "LoginForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LoginForm"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents userNameTextBox As TextBox
    Friend WithEvents passwordTextBox As TextBox
    Friend WithEvents loginButton As Button
    Friend WithEvents loginCancelButton As Button
    Friend WithEvents timeLabel As Label
    Friend WithEvents Timer1 As Timer
End Class
