﻿Namespace Products
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductUpdateForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProductUpdateForm))
        Me.productsGrid = New System.Windows.Forms.DataGridView()
        Me.ColumnProductId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductRegistrationDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductState = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.productGroupBox = New System.Windows.Forms.GroupBox()
        Me.quantityTextBox = New System.Windows.Forms.TextBox()
        Me.priceTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.descriptionTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.editButton = New System.Windows.Forms.Button()
        Me.editAcceptButton = New System.Windows.Forms.Button()
        Me.editCancelButton = New System.Windows.Forms.Button()
        Me.errorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.productsGrid,System.ComponentModel.ISupportInitialize).BeginInit
        Me.productGroupBox.SuspendLayout
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'productsGrid
        '
        Me.productsGrid.AllowUserToAddRows = false
        Me.productsGrid.AllowUserToDeleteRows = false
        Me.productsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.productsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnProductId, Me.ColumnProductName, Me.ColumnProductRegistrationDate, Me.ColumnProductPrice, Me.ColumnProductQuantity, Me.ColumnProductState})
        Me.productsGrid.Location = New System.Drawing.Point(7, 7)
        Me.productsGrid.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.productsGrid.Name = "productsGrid"
        Me.productsGrid.ReadOnly = true
        Me.productsGrid.RowHeadersVisible = false
        Me.productsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.productsGrid.Size = New System.Drawing.Size(671, 134)
        Me.productsGrid.TabIndex = 4
        '
        'ColumnProductId
        '
        Me.ColumnProductId.DataPropertyName = "Id"
        Me.ColumnProductId.HeaderText = "Id"
        Me.ColumnProductId.Name = "ColumnProductId"
        Me.ColumnProductId.ReadOnly = true
        Me.ColumnProductId.Visible = false
        '
        'ColumnProductName
        '
        Me.ColumnProductName.DataPropertyName = "Name"
        Me.ColumnProductName.HeaderText = "Descripcion"
        Me.ColumnProductName.Name = "ColumnProductName"
        Me.ColumnProductName.ReadOnly = true
        Me.ColumnProductName.Width = 393
        '
        'ColumnProductRegistrationDate
        '
        Me.ColumnProductRegistrationDate.DataPropertyName = "RegistrationDate"
        Me.ColumnProductRegistrationDate.HeaderText = "Fecha Registrado"
        Me.ColumnProductRegistrationDate.Name = "ColumnProductRegistrationDate"
        Me.ColumnProductRegistrationDate.ReadOnly = true
        Me.ColumnProductRegistrationDate.Width = 122
        '
        'ColumnProductPrice
        '
        Me.ColumnProductPrice.DataPropertyName = "Price"
        Me.ColumnProductPrice.HeaderText = "Precio"
        Me.ColumnProductPrice.Name = "ColumnProductPrice"
        Me.ColumnProductPrice.ReadOnly = true
        Me.ColumnProductPrice.Width = 50
        '
        'ColumnProductQuantity
        '
        Me.ColumnProductQuantity.DataPropertyName = "Quantity"
        Me.ColumnProductQuantity.HeaderText = "Cantidad"
        Me.ColumnProductQuantity.Name = "ColumnProductQuantity"
        Me.ColumnProductQuantity.ReadOnly = true
        Me.ColumnProductQuantity.Width = 50
        '
        'ColumnProductState
        '
        Me.ColumnProductState.DataPropertyName = "State"
        Me.ColumnProductState.HeaderText = "Activo"
        Me.ColumnProductState.Name = "ColumnProductState"
        Me.ColumnProductState.ReadOnly = true
        Me.ColumnProductState.Width = 50
        '
        'productGroupBox
        '
        Me.productGroupBox.Controls.Add(Me.quantityTextBox)
        Me.productGroupBox.Controls.Add(Me.priceTextBox)
        Me.productGroupBox.Controls.Add(Me.Label3)
        Me.productGroupBox.Controls.Add(Me.Label2)
        Me.productGroupBox.Controls.Add(Me.descriptionTextBox)
        Me.productGroupBox.Controls.Add(Me.Label1)
        Me.productGroupBox.Location = New System.Drawing.Point(84, 149)
        Me.productGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.productGroupBox.Name = "productGroupBox"
        Me.productGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.productGroupBox.Size = New System.Drawing.Size(516, 115)
        Me.productGroupBox.TabIndex = 5
        Me.productGroupBox.TabStop = false
        '
        'quantityTextBox
        '
        Me.quantityTextBox.Enabled = false
        Me.quantityTextBox.Location = New System.Drawing.Point(407, 84)
        Me.quantityTextBox.Name = "quantityTextBox"
        Me.quantityTextBox.Size = New System.Drawing.Size(100, 20)
        Me.quantityTextBox.TabIndex = 7
        '
        'priceTextBox
        '
        Me.priceTextBox.Enabled = false
        Me.priceTextBox.Location = New System.Drawing.Point(407, 33)
        Me.priceTextBox.Name = "priceTextBox"
        Me.priceTextBox.Size = New System.Drawing.Size(100, 20)
        Me.priceTextBox.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(407, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Cantidad"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(407, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Precio"
        '
        'descriptionTextBox
        '
        Me.descriptionTextBox.Enabled = false
        Me.descriptionTextBox.Location = New System.Drawing.Point(6, 33)
        Me.descriptionTextBox.Multiline = true
        Me.descriptionTextBox.Name = "descriptionTextBox"
        Me.descriptionTextBox.Size = New System.Drawing.Size(395, 71)
        Me.descriptionTextBox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(6, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Descripcion"
        '
        'editButton
        '
        Me.editButton.Image = CType(resources.GetObject("editButton.Image"),System.Drawing.Image)
        Me.editButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.editButton.Location = New System.Drawing.Point(146, 270)
        Me.editButton.Name = "editButton"
        Me.editButton.Size = New System.Drawing.Size(120, 58)
        Me.editButton.TabIndex = 8
        Me.editButton.Text = "Editar"
        Me.editButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.editButton.UseVisualStyleBackColor = true
        '
        'editAcceptButton
        '
        Me.editAcceptButton.Enabled = false
        Me.editAcceptButton.Image = CType(resources.GetObject("editAcceptButton.Image"),System.Drawing.Image)
        Me.editAcceptButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.editAcceptButton.Location = New System.Drawing.Point(274, 270)
        Me.editAcceptButton.Name = "editAcceptButton"
        Me.editAcceptButton.Size = New System.Drawing.Size(120, 58)
        Me.editAcceptButton.TabIndex = 9
        Me.editAcceptButton.Text = "Aceptar"
        Me.editAcceptButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.editAcceptButton.UseVisualStyleBackColor = true
        '
        'editCancelButton
        '
        Me.editCancelButton.Enabled = false
        Me.editCancelButton.Image = CType(resources.GetObject("editCancelButton.Image"),System.Drawing.Image)
        Me.editCancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.editCancelButton.Location = New System.Drawing.Point(402, 270)
        Me.editCancelButton.Name = "editCancelButton"
        Me.editCancelButton.Size = New System.Drawing.Size(120, 58)
        Me.editCancelButton.TabIndex = 10
        Me.editCancelButton.Text = "Cancelar"
        Me.editCancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.editCancelButton.UseVisualStyleBackColor = true
        '
        'errorProvider
        '
        Me.errorProvider.ContainerControl = Me
        '
        'ProductUpdateForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.ClientSize = New System.Drawing.Size(684, 340)
        Me.Controls.Add(Me.editCancelButton)
        Me.Controls.Add(Me.productGroupBox)
        Me.Controls.Add(Me.editAcceptButton)
        Me.Controls.Add(Me.productsGrid)
        Me.Controls.Add(Me.editButton)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "ProductUpdateForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Products Update"
        CType(Me.productsGrid,System.ComponentModel.ISupportInitialize).EndInit
        Me.productGroupBox.ResumeLayout(false)
        Me.productGroupBox.PerformLayout
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

        Friend WithEvents productsGrid As DataGridView
        Friend WithEvents productGroupBox As GroupBox
        Friend WithEvents quantityTextBox As TextBox
        Friend WithEvents priceTextBox As TextBox
        Friend WithEvents Label3 As Label
        Friend WithEvents Label2 As Label
        Friend WithEvents descriptionTextBox As TextBox
        Friend WithEvents Label1 As Label
        Friend WithEvents editButton As Button
        Friend WithEvents editAcceptButton As Button
        Friend WithEvents editCancelButton As Button
        Friend WithEvents errorProvider As ErrorProvider
        Friend WithEvents ColumnProductId As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductRegistrationDate As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductPrice As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductQuantity As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductState As DataGridViewCheckBoxColumn
    End Class
End NameSpace