﻿Imports LayerPresentation.Products
Imports LayerPresentation.Users
Imports LayerPresentation.Stock
Public Class MainForm

    Friend Property UserName As String
        Get
            Return userNameStatusLabel.Text
        End Get
        Set 
            userNameStatusLabel.Text = value
        End Set
    End Property

    Friend Property UserFullName As String
        Get
            Return userFullNameStatusLabel.Text
        End Get
        Set 
            userFullNameStatusLabel.Text = value
        End Set
    End Property

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim login As New LoginForm
        If login.ShowDialog() = DialogResult.Cancel
            Close()
        End If
    End Sub

    Private Sub tsmi_listProducts_Click(sender As Object, e As EventArgs) Handles tsmi_listProducts.Click
        With ProductListForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_NewProduct_Click(sender As Object, e As EventArgs) Handles tsmi_NewProduct.Click
        With ProductCreateForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_UpdateProduct_Click(sender As Object, e As EventArgs) Handles tsmi_UpdateProduct.Click
        With ProductUpdateForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_deletProduct_Click(sender As Object, e As EventArgs) Handles tsmi_deletProduct.Click
        With ProductDeleteForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_ListUser_Click(sender As Object, e As EventArgs) Handles tsmi_ListUser.Click
        With UserListForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_addUsers_Click(sender As Object, e As EventArgs) Handles tsmi_addUsers.Click
        With UserCreateForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_UpdateUsers_Click(sender As Object, e As EventArgs) Handles tsmi_UpdateUsers.Click
        With UserUpdateForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_deleteUser_Click(sender As Object, e As EventArgs) Handles tsmi_deleteUser.Click
        With UserDeleteForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_intoProducstStock_Click(sender As Object, e As EventArgs) Handles tsmi_intoProducstStock.Click
        With IncrementStockForm
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub tsmi_outProductsStock_Click(sender As Object, e As EventArgs) Handles tsmi_outProductsStock.Click
        With DiscountStockForm
            .MdiParent = Me
            .Show()
        End With
    End Sub
End Class