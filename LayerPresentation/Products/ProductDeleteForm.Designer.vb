﻿Namespace Products
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductDeleteForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProductDeleteForm))
        Me.productsGrid = New System.Windows.Forms.DataGridView()
        Me.ColumnProductId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductRegistrationDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductState = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.productGroupBox = New System.Windows.Forms.GroupBox()
        Me.quantityTextBox = New System.Windows.Forms.TextBox()
        Me.priceTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.descriptionTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.deleteCancelButton = New System.Windows.Forms.Button()
        Me.deleteAcceptButton = New System.Windows.Forms.Button()
        Me.deleteButton = New System.Windows.Forms.Button()
        CType(Me.productsGrid,System.ComponentModel.ISupportInitialize).BeginInit
        Me.productGroupBox.SuspendLayout
        Me.SuspendLayout
        '
        'productsGrid
        '
        Me.productsGrid.AllowUserToAddRows = false
        Me.productsGrid.AllowUserToDeleteRows = false
        Me.productsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.productsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnProductId, Me.ColumnProductName, Me.ColumnProductRegistrationDate, Me.ColumnProductPrice, Me.ColumnProductQuantity, Me.ColumnProductState})
        Me.productsGrid.Location = New System.Drawing.Point(5, 6)
        Me.productsGrid.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.productsGrid.Name = "productsGrid"
        Me.productsGrid.ReadOnly = true
        Me.productsGrid.RowHeadersVisible = false
        Me.productsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.productsGrid.Size = New System.Drawing.Size(671, 134)
        Me.productsGrid.TabIndex = 5
        '
        'ColumnProductId
        '
        Me.ColumnProductId.DataPropertyName = "Id"
        Me.ColumnProductId.HeaderText = "Id"
        Me.ColumnProductId.Name = "ColumnProductId"
        Me.ColumnProductId.ReadOnly = true
        Me.ColumnProductId.Visible = false
        '
        'ColumnProductName
        '
        Me.ColumnProductName.DataPropertyName = "Name"
        Me.ColumnProductName.HeaderText = "Descripcion"
        Me.ColumnProductName.Name = "ColumnProductName"
        Me.ColumnProductName.ReadOnly = true
        Me.ColumnProductName.Width = 393
        '
        'ColumnProductRegistrationDate
        '
        Me.ColumnProductRegistrationDate.DataPropertyName = "RegistrationDate"
        Me.ColumnProductRegistrationDate.HeaderText = "Fecha Registrado"
        Me.ColumnProductRegistrationDate.Name = "ColumnProductRegistrationDate"
        Me.ColumnProductRegistrationDate.ReadOnly = true
        Me.ColumnProductRegistrationDate.Width = 122
        '
        'ColumnProductPrice
        '
        Me.ColumnProductPrice.DataPropertyName = "Price"
        Me.ColumnProductPrice.HeaderText = "Precio"
        Me.ColumnProductPrice.Name = "ColumnProductPrice"
        Me.ColumnProductPrice.ReadOnly = true
        Me.ColumnProductPrice.Width = 50
        '
        'ColumnProductQuantity
        '
        Me.ColumnProductQuantity.DataPropertyName = "Quantity"
        Me.ColumnProductQuantity.HeaderText = "Cantidad"
        Me.ColumnProductQuantity.Name = "ColumnProductQuantity"
        Me.ColumnProductQuantity.ReadOnly = true
        Me.ColumnProductQuantity.Width = 50
        '
        'ColumnProductState
        '
        Me.ColumnProductState.DataPropertyName = "State"
        Me.ColumnProductState.HeaderText = "Activo"
        Me.ColumnProductState.Name = "ColumnProductState"
        Me.ColumnProductState.ReadOnly = true
        Me.ColumnProductState.Width = 50
        '
        'productGroupBox
        '
        Me.productGroupBox.Controls.Add(Me.quantityTextBox)
        Me.productGroupBox.Controls.Add(Me.priceTextBox)
        Me.productGroupBox.Controls.Add(Me.Label3)
        Me.productGroupBox.Controls.Add(Me.Label2)
        Me.productGroupBox.Controls.Add(Me.descriptionTextBox)
        Me.productGroupBox.Controls.Add(Me.Label1)
        Me.productGroupBox.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.productGroupBox.Location = New System.Drawing.Point(82, 145)
        Me.productGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.productGroupBox.Name = "productGroupBox"
        Me.productGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.productGroupBox.Size = New System.Drawing.Size(516, 115)
        Me.productGroupBox.TabIndex = 6
        Me.productGroupBox.TabStop = false
        '
        'quantityTextBox
        '
        Me.quantityTextBox.Enabled = false
        Me.quantityTextBox.Location = New System.Drawing.Point(407, 84)
        Me.quantityTextBox.Name = "quantityTextBox"
        Me.quantityTextBox.Size = New System.Drawing.Size(100, 20)
        Me.quantityTextBox.TabIndex = 7
        '
        'priceTextBox
        '
        Me.priceTextBox.Enabled = false
        Me.priceTextBox.Location = New System.Drawing.Point(407, 33)
        Me.priceTextBox.Name = "priceTextBox"
        Me.priceTextBox.Size = New System.Drawing.Size(100, 20)
        Me.priceTextBox.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(407, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Cantidad"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(407, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Precio"
        '
        'descriptionTextBox
        '
        Me.descriptionTextBox.Enabled = false
        Me.descriptionTextBox.Location = New System.Drawing.Point(6, 33)
        Me.descriptionTextBox.Multiline = true
        Me.descriptionTextBox.Name = "descriptionTextBox"
        Me.descriptionTextBox.Size = New System.Drawing.Size(395, 71)
        Me.descriptionTextBox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(6, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Descripcion"
        '
        'deleteCancelButton
        '
        Me.deleteCancelButton.Enabled = false
        Me.deleteCancelButton.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.deleteCancelButton.Image = CType(resources.GetObject("deleteCancelButton.Image"),System.Drawing.Image)
        Me.deleteCancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.deleteCancelButton.Location = New System.Drawing.Point(395, 267)
        Me.deleteCancelButton.Name = "deleteCancelButton"
        Me.deleteCancelButton.Size = New System.Drawing.Size(120, 62)
        Me.deleteCancelButton.TabIndex = 13
        Me.deleteCancelButton.Text = "Cancelar"
        Me.deleteCancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.deleteCancelButton.UseVisualStyleBackColor = true
        '
        'deleteAcceptButton
        '
        Me.deleteAcceptButton.Enabled = false
        Me.deleteAcceptButton.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.deleteAcceptButton.Image = CType(resources.GetObject("deleteAcceptButton.Image"),System.Drawing.Image)
        Me.deleteAcceptButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.deleteAcceptButton.Location = New System.Drawing.Point(267, 267)
        Me.deleteAcceptButton.Name = "deleteAcceptButton"
        Me.deleteAcceptButton.Size = New System.Drawing.Size(120, 62)
        Me.deleteAcceptButton.TabIndex = 12
        Me.deleteAcceptButton.Text = "Aceptar"
        Me.deleteAcceptButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.deleteAcceptButton.UseVisualStyleBackColor = true
        '
        'deleteButton
        '
        Me.deleteButton.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.deleteButton.Image = CType(resources.GetObject("deleteButton.Image"),System.Drawing.Image)
        Me.deleteButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.deleteButton.Location = New System.Drawing.Point(139, 267)
        Me.deleteButton.Name = "deleteButton"
        Me.deleteButton.Size = New System.Drawing.Size(120, 62)
        Me.deleteButton.TabIndex = 11
        Me.deleteButton.Text = "Eliminar"
        Me.deleteButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.deleteButton.UseVisualStyleBackColor = true
        '
        'ProductDeleteForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(681, 334)
        Me.Controls.Add(Me.deleteCancelButton)
        Me.Controls.Add(Me.deleteAcceptButton)
        Me.Controls.Add(Me.deleteButton)
        Me.Controls.Add(Me.productGroupBox)
        Me.Controls.Add(Me.productsGrid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "ProductDeleteForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Product Delete"
        CType(Me.productsGrid,System.ComponentModel.ISupportInitialize).EndInit
        Me.productGroupBox.ResumeLayout(false)
        Me.productGroupBox.PerformLayout
        Me.ResumeLayout(false)

End Sub

        Friend WithEvents productsGrid As DataGridView
        Friend WithEvents productGroupBox As GroupBox
        Friend WithEvents quantityTextBox As TextBox
        Friend WithEvents priceTextBox As TextBox
        Friend WithEvents Label3 As Label
        Friend WithEvents Label2 As Label
        Friend WithEvents descriptionTextBox As TextBox
        Friend WithEvents Label1 As Label
        Friend WithEvents deleteCancelButton As Button
        Friend WithEvents deleteAcceptButton As Button
        Friend WithEvents deleteButton As Button
        Friend WithEvents ColumnProductId As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductRegistrationDate As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductPrice As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductQuantity As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductState As DataGridViewCheckBoxColumn
    End Class
End NameSpace